﻿-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.31 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных company
CREATE DATABASE IF NOT EXISTS `company` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `company`;


-- Дамп структуры для таблица company.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` char(50) NOT NULL,
  `lastname` char(50) NOT NULL,
  `title` text NOT NULL,
  `age` tinyint(4) NOT NULL DEFAULT '0',
  `salary` decimal(20,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы company.employees: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `firstname`, `lastname`, `title`, `age`, `salary`) VALUES
	(1, 'Jonie', 'Weber', 'Secretary', 28, 19500),
	(2, 'Potsy', 'Weber', 'Programmer', 32, 45300),
	(3, 'Dirk', 'Smith', 'Programmer II', 45, 75020),
	(4, 'Sergey', 'Bure', 'Programmer', 30, 60550),
	(5, 'Vadim', 'Korik', 'System architect', 18, 145000),
	(6, 'Anny', 'Typicova', 'Project manages', 82, 9300),
	(7, 'Sveta', 'Lisovay', 'Front-end developer', 35, 20000),
	(8, 'George', 'Carling', 'Security', 40, 8000);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

SELECT * FROM employees;

SELECT * FROM employees WHERE salary > 30000;

SELECT firstname, lastname FROM employees WHERE age < 30;

SELECT firstname, lastname, salary FROM employees WHERE title LIKE %Programmer%;

SELECT * FROM employees WHERE lastname LIKE %ebe%;

SELECT firstname FROM employees WHERE firstname = Potsy;

SELECT * FROM employees WHERE age > 80;

SELECT * FROM employees WHERE lastname LIKE %ith;
